from django.urls import path, re_path, register_converter
from . import views, route_convertors

register_converter(route_convertors.FourDigitConv, 'YYYY')

urlpatterns = [
    path("", views.index),
    path('articles/2003/', views.special_case_2003),
    path('articles/<YYYY:year>', views.year_archive),
    # re_path('articles/(?P<year>[0-9]{4})/$', views.year_archive),
    path('articles/<int:year>/<int:month>/', views.month_archive),
    path('articles/<int:year>/<int:month>/<slug:slug>/', views.article_detail),
    path("articles/<path:route>", views.rout_view)
]

# str виключаючи "/"
# int
# slug 111ddd_66-yu5
# UUID
# path
