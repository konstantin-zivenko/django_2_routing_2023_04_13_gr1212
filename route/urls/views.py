from django.http import HttpRequest, HttpResponse


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("it is main page")


def special_case_2003(request: HttpRequest) -> HttpResponse:
    return HttpResponse('special case 2003')


def year_archive(requests: HttpRequest, year: int | None) -> HttpResponse:
    return HttpResponse(f'year archive: {year}')


def month_archive(requests: HttpRequest, year: int | None, month: int | None) -> HttpResponse:
    return HttpResponse(f'year archive: {year} month {month}')


def article_detail(requests: HttpRequest, year: int | None, month: int | None, slug: str | None) -> HttpResponse:
    return HttpResponse(f'year archive: {year} month {month} \n title: {slug}')


def rout_view(requests: HttpRequest, route):
    return HttpResponse(f"----> {route}")
